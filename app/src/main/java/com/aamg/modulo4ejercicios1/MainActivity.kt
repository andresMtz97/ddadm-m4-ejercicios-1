package com.aamg.modulo4ejercicios1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios1.databinding.ActivityMainBinding
import com.aamg.modulo4ejercicios1.ejercicio1.Ejercicio1Activity
import com.aamg.modulo4ejercicios1.ejercicio2.Ejercicio2Activity
import com.aamg.modulo4ejercicios1.ejercicio3.SendExtrasActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCicloVida.setOnClickListener { navigateToEjercicio1() }
        binding.btnIntentImplicito.setOnClickListener { navigateToEjercicio2() }
        binding.btnPasoParametros.setOnClickListener { navigateToEjercicio3() }
    }

    private fun navigateToEjercicio1() {
        val intent = Intent(this, Ejercicio1Activity::class.java)
        startActivity(intent)
    }

    private fun navigateToEjercicio2() {
        val intent = Intent(this, Ejercicio2Activity::class.java)
        startActivity(intent)
    }

    private fun navigateToEjercicio3() {
        val intent = Intent(this, SendExtrasActivity::class.java)
        startActivity(intent)
    }
}