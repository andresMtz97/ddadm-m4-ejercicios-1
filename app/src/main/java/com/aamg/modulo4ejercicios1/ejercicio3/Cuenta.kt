package com.aamg.modulo4ejercicios1.ejercicio3

import java.io.Serializable

class Cuenta (
    val cuentaId: Int,
    val nombre: String,
    val saldo: Double
): Serializable