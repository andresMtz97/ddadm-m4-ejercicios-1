package com.aamg.modulo4ejercicios1.ejercicio3

import java.io.Serializable

class Usuario (
    val name: String,
    val lastname: String,
    val username: String
): Serializable