package com.aamg.modulo4ejercicios1.ejercicio2

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aamg.modulo4ejercicios1.databinding.ActivityEjercicio2Binding

class Ejercicio2Activity : AppCompatActivity() {

    private lateinit var binding: ActivityEjercicio2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEjercicio2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnWeb.setOnClickListener { navigateToWeb() }
        binding.btnFb.setOnClickListener { navigateToFb() }
        binding.btnX.setOnClickListener { navigateToX() }
        binding.btnIg.setOnClickListener { navigateToInstagram() }
        binding.btnTikTok.setOnClickListener { navigateToTikTok() }
        binding.btnYouTube.setOnClickListener { navigateToYouTube() }
    }

    private fun navigateToWeb() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.crunchyroll.com/es/"))
        startActivity(Intent.createChooser(intent, "Crunchyroll web"))
    }

    private fun navigateToFb() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Crunchyroll.la/"))
        startActivity(Intent.createChooser(intent, "Facebook"))
    }

    private fun navigateToX() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/crunchyroll_la"))
        startActivity(Intent.createChooser(intent, "X"))
    }

    private fun navigateToInstagram() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/crunchyroll_la/"))
        startActivity(Intent.createChooser(intent, "Instagram"))
    }

    private fun navigateToTikTok() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.tiktok.com/@crunchyroll_la"))
        startActivity(Intent.createChooser(intent, "TikTok"))
    }

    private fun navigateToYouTube() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/user/crunchyrollspanish/featured"))
        startActivity(Intent.createChooser(intent, "YouTube"))
    }
}